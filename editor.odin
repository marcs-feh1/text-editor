package text_editor

import str "core:strings"

Buffer_Flag :: enum {
	ReadOnly,
	Scratch,
}

Edit_Mode :: enum {
	Normal = 0,
	Insert,
	Select,
}

Buffer_Flags :: bit_set[Buffer_Flag]

Editor_Buffer :: struct {
	text: Piece_Table,
	cursor: Cursor,
	mark: Cursor,

	// Metadata
	filename: string,
	flags: Buffer_Flags,
	mode: Edit_Mode,
}

Editor :: struct {
	buffers: [dynamic]Editor_Buffer,
	active_buffer: int, // User focused buffer, mostly for UI usage
}

editor_make :: proc(allocator := context.allocator) -> Editor {
	ed := Editor {
		buffers = make([dynamic]Editor_Buffer),
		active_buffer = -1,
	}
	return ed
}

editor_open_buffer :: proc(ed: ^Editor, filename: string, flags : Buffer_Flags, set_active := false) {
	// TODO: open a file, or optionally create scratch buf
	table := table_make("")
	was_empty := len(ed.buffers) == 0

	name, err := str.clone(filename)
	assert(err == nil, "Failed to open buffer")

	buf := Editor_Buffer{
		text = table,
		filename = name,
	}

	append(&ed.buffers, buf)

	if set_active || was_empty {
		ed.active_buffer = len(ed.buffers) - 1
	}
}

editor_close_buffer :: proc(ed: ^Editor, which: i32){
	table_delete(&ed.buffers[which].text)
	delete(ed.buffers[which].filename)
	ordered_remove(&ed.buffers, int(which))
}

