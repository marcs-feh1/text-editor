package text_editor

import utf "core:unicode/utf8"

utf8_size_category :: proc(b: byte) -> int {
	b := u32(b)
	switch {
	case (b & ~u32(utf.MASK2)) == utf.T1: return 1
	case (b & ~u32(utf.MASK2)) == utf.T2: return 2
	case (b & ~u32(utf.MASK2)) == utf.T3: return 3
	case (b & ~u32(utf.MASK2)) == utf.T4: return 4
	}
	return 0
}
