package text_editor

import utf "core:unicode/utf8"

Cursor :: struct {
	piece, offset: int,
}

cursor_next_rune :: proc(table: Piece_Table, cur: Cursor) -> Cursor {
	cur := cur
	piece := table.entries[cur.piece]
	if piece.kind == .EOT { return cur }

	data := table_read_piece(table, cur.piece)
	_, n := utf.decode_rune(data)
	cur.offset += n

	assert(n > 0, "Rune decoding error")

	if cur.offset >= piece.size {
		cur.piece += 1
		cur.offset = 0
	}

	return cur
}

@private
is_continuation_byte :: proc(b: byte) -> bool {
	return (b & utf.MASKX) == 0b1000_0000
}

cursor_prev_rune :: proc(table: Piece_Table, cur: Cursor) -> Cursor {
	cur := cur

	if cur.offset == 0 && cur.piece == 0 {
		return cur
	}

	read_previous := cur.offset == 0
	piece_idx := cur.piece - int(read_previous)
	data := table_read_piece(table, piece_idx)

	n : int
	if read_previous {
		n = table.entries[piece_idx].size - 1
	}
	else {
		n = cur.offset - 1
	}

	for n > 0 {
		if !is_continuation_byte(data[n]){
			break
		}
		n -= 1
	}

	return Cursor {
		offset = max(0, n),
		piece = cur.piece - int(read_previous),
	}
}

table_get_byte :: proc {
	table_get_byte_with_cursor,
	table_get_byte_with_offset,
}

table_get_byte_with_cursor :: proc(table: Piece_Table, cur: Cursor) -> byte {
	segment := table_read_piece(table, cur.piece)
	return segment[cur.offset]
}

table_get_cursor_piece :: proc(table: Piece_Table, cur: Cursor) -> ^Piece {
	return &table.entries[min(cur.piece, len(table.entries) - 1)]
}

// Reads a rune under cursor
rune_under_cursor :: proc(table: Piece_Table, cur: Cursor) -> (rune, int) {
	p := table_get_cursor_piece(table, cur)
	if p.kind == .EOT {
		return utf.RUNE_EOF, 1
	}
	segment := table_read_piece(table, cur.piece)
	r, n := utf.decode_rune(string(segment[cur.offset:]))
	return r, n
}

table_get_byte_with_offset :: proc(table: Piece_Table, idx: int) -> byte {
	cur := cursor_from_offset(table, idx)
	return table_get_byte_with_cursor(table, cur)
}

cursor_from :: proc {
	cursor_from_offset,
	// TODO: Visual position
}

cursor_from_offset :: proc(table: Piece_Table, idx: int) -> Cursor {
	cur := cursor_move_bytes(table, Cursor{0, 0}, idx)
	return cur
}

offset_from_cursor :: proc(table: Piece_Table, cur: Cursor) -> int {
	off := 0

	for i in 0..<cur.piece {
		off += table.entries[i].size
	}
	off += cur.offset
	return off
}

// cursor_in_bounds :: proc(table: Piece_Table, cur: Cursor) -> bool {
// 	ok := ((cur == table_last(table)) ||
// 		(cur.piece < len(table.entries)) &&
// 		(cur.offset < table.entries[cur.piece].size))
// 	return ok
// }

// Move cursor by `delta` bytes
cursor_move_bytes :: proc(table: Piece_Table, cur: Cursor, delta: int) -> Cursor {
	if delta == 0 { return cur }

	begin :: Cursor{0, 0}
	end := table_last(table)

	cur := cur
	p := table.entries[cur.piece]
	if delta > 0 {
		cur.offset += delta

		for (cur.offset >= p.size){
			cur.piece += 1

			// Subtract size of *current*
			cur.offset -= p.size
			if p.kind == .EOT { return end }
			p = table.entries[cur.piece]
		}
	}
	else {
		cur.offset -= abs(delta)

		for cur.offset < 0  {
			cur.piece -= 1

			if cur.piece < 0 { return begin }
			// Subtract size of *previous*
			p = table.entries[cur.piece]
			cur.offset += p.size
		}
	}

	assert(cur.offset < p.size)
	return cur
}
