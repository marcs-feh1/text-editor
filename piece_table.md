# Piece table

A piece table is composed of 2 buffers, the Original buffer and the Append
buffer. Nothing can be changed in the Original buffer, it's always read-only.
The Append buffer is generally append-only, with one optimization exception for
interactive usage.

The piece table is composed of pieces, a Piece is simply a view into one of the
2 buffers. The `sequence` is not a literal property of the data structure, but
the final text that it represents.

# Creation

Assume the source string "This is text", the resulting table would be as follows.

```
original: [This is text]
append:   []
sequence: [This is text]

table:
  | kind     | start | length |
  |.Original | 0     | 12     |

```

# Inserting text
To insert text at a cursor, a new piece is usually created. Assume we want to have the string "This is more text"

```
original: [This is text]
append:   [ more]
sequence: [This is more text]

table:
  | kind     | start | length |
  |.Original | 0     | 7      |
  |.Append   | 0     | 5      |
  |.Original | 7     | 5      |
```


## Inserting text: Special interactive case

# Deleting text


	// A: Trivial case, in-piece deletion
	//   v..x
	// |      |    |    |             |    |

	// B: Adjecent, transform into 2 A cases
	//      v....x
	// |      |    |    |             |    |

	// C: Multi piece, remove whole middle pieces, goto B
	//     v..............x
	// |      |    |    |             |    |
