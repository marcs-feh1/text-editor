package text_editor

import "core:mem"
import "core:slice"

import "core:fmt"

Piece_Table :: struct {
	entries: [dynamic]Piece,
	append_buf: [dynamic]byte,
	original_buf: []byte,
}

Piece :: struct {
	using range: Range,
	kind: Buffer_Kind,
}

Buffer_Kind :: enum i8 {
	// Original text, read-only.
	Original,

	// Inserted text, append-only with the  exception of 1 edge case to
	// optimize for interactive use where it can be pop+append.
	Append,

	// Indicates the end of table, there's always *one* end of table piece, it
	// shall always be the last one.
	EOT,
}

Range :: struct {
	start, size: int,
}

table_make :: proc {
	table_make_string,
	table_make_bytes,
}

table_delete :: proc(table: ^Piece_Table) {
	table.original_buf = nil
	delete(table.entries)
	delete(table.append_buf)
}

table_make_string :: proc(original_data: string, allocator := context.allocator) -> (table: Piece_Table) {
	return table_make_bytes(string_to_bytes_unsafe(original_data), allocator = allocator)
}

table_make_bytes :: proc(original_data: []byte, allocator := context.allocator) -> (table: Piece_Table) {
	table.entries = make([dynamic]Piece)
	table.append_buf = make([dynamic]byte)
	table.original_buf = original_data
	if len(original_data) > 0 {
		append(&table.entries, Piece{{0, len(original_data)}, .Original})
	}
	append(&table.entries, Piece{kind = .EOT})
	return
}

// Creates a new buffer with the contents that the piece table represents.
table_commit :: proc(table: Piece_Table) -> (contents: []byte, err: mem.Allocator_Error) {
	size := table_size(table)
	contents = make([]byte, size) or_return

	off := 0
	// Don't include EOT
	entries := table.entries[:len(table.entries) - 1]
	for _, i in entries {
		data := table_read_piece(table, i)
		mem.copy(&contents[off], raw_data(data), len(data))
		off += len(data)
	}

	return
}

has_eot :: proc(table: Piece_Table) -> bool {
	return len(table.entries) > 0 && table.entries[len(table.entries) - 1].kind == .EOT
}

// Pop one rune from under cursor
table_pop_rune :: proc(table: ^Piece_Table, cur: Cursor){
}

table_del_text :: proc(table: ^Piece_Table, start, end: Cursor){
}

// TODO: cache this?
table_size :: proc(table: Piece_Table) -> int {
	acc := 0
	for e in table.entries {
		acc += e.size
	}
	return acc
}

// Similar to table_inject_text, but doesn't always create a new piece if piece
// that is receiving the insertion coincides with end of append buffer.
table_add_text :: proc {
	table_add_text_bytes,
	table_add_text_str,
}

table_add_text_bytes :: proc(table: ^Piece_Table, data: []byte, cursor: Cursor) -> (in_place := false) {
	cur_piece := table_get_cursor_piece(table^, cursor)
	if cursor.piece > 0 {
		prev_piece := table.entries[cursor.piece - 1]

		append_in_place := (
			cursor.offset == 0 &&
			prev_piece.kind == .Append &&
			end_of_append_buf(table^, cursor.piece - 1))

		if append_in_place {
			table_piece_append_in_place(table, data, cursor.piece - 1)
			in_place = true
		}
		else {
			table_inject_text(table, data, cursor)
		}
	}
	else {
		table_inject_text(table, data, cursor)
	}
	return
}

table_add_text_str :: proc(table: ^Piece_Table, data: string, cursor: Cursor) {
	data := string_to_bytes_unsafe(data)
	table_add_text_bytes(table, data, cursor)
}

@private
end_of_append_buf :: proc(table:Piece_Table, idx: int) -> bool {
	piece := table.entries[idx]
	return (piece.start + piece.size) == len(table.append_buf)
}

// @private
// can_append_in_place :: proc(table: Piece_Table, cur: Cursor) -> bool {
// 	piece             := table.entries[cur.piece]
// 	end_of_append_buf := (piece.start + piece.size) == len(table.append_buf)
// 	end_of_piece      := cur.offset == piece.size
// 	can_append        := end_of_piece && end_of_append_buf && (piece.kind == .Append)
// 	return can_append
// }

// Insert text at cursor, this always creates a new piece
table_inject_text :: proc {
	table_inject_text_bytes,
	table_inject_text_string,
}

// Get index of a piece, assumes piece is part of table.
@private
get_piece_index :: proc(table: Piece_Table, p: ^Piece) -> int {
	base := uintptr(&table.entries[0])
	off := uintptr(p)
	return int(off - base) / size_of(Piece)
}

// Inject text at a particular cursor, this function always creates a new piece
// after splitting, returns updated cursor position
table_inject_text_bytes :: proc(table: ^Piece_Table, data: []byte, cur: Cursor) -> Cursor {
	if len(data) < 1 { return cur }

	if table.entries[cur.piece].kind == .EOT {
		table_add_piece(table, cur.piece, data)
		return cur
	}

	// NOTE: Cursor is now on `first`, but with an incorrect offset
	first, second := table_split_piece(table, cur.piece, cur.offset)

	first_idx := get_piece_index(table^, first)
	second_idx := get_piece_index(table^, second)

	table_add_piece(table, second_idx, data)

	new_cur := cursor_move_bytes(table^, cur, len(data))

	// NOTE: It's not possible for both pieces to be empty because of how the
	// splitting logic happens.
	if first.size < 1 {
		table_pop_piece(table, first_idx)
		new_cur.piece -= 1
	}
	else if second.size < 1 {
		table_pop_piece(table, second_idx + 1)
	}

	return new_cur
}

// Remove all zero-sized pieces
table_gc :: proc(table: ^Piece_Table){
	#reverse for p, i in table.entries {
		if p.size == 0 {
			ordered_remove(&table.entries, i)
		}
	}
	// EOT was removed as its size is always zero, so put it back
	append(&table.entries, Piece{ kind = .EOT })
}

// Insert text at cursor, this always creates a new piece
table_inject_text_string :: proc(table: ^Piece_Table, data: string, cur: Cursor) -> Cursor {
	data := string_to_bytes_unsafe(data)
	return table_inject_text_bytes(table, data, cur)
}

// Last position that is inside a piece in the table
table_last :: proc(table: Piece_Table) -> Cursor {
	assert(len(table.entries) > 0, "Empty table")
	n := len(table.entries) - 1
	return Cursor {
		piece = n,
		offset = table.entries[n].size,
	}
}

// Append bytes to append buffer and inject piece corresponding to piece_idx
table_add_piece :: proc(table: ^Piece_Table, idx: int, data: []byte){
	old_len := len(table.append_buf)
	append(&table.append_buf, ..data)
	piece := Piece {
		start = old_len,
		size = len(data),
		kind = .Append,
	}
	inject_at(&table.entries, idx, piece)
}

// Remove piece from piece table
table_pop_piece :: proc(table: ^Piece_Table, piece_idx: int) {
	assert(table.entries[piece_idx].kind != .EOT, "The End of table piece cannot be deleted.")
	ordered_remove(&table.entries, piece_idx)
}

@private
table_piece_append_in_place :: proc(table: ^Piece_Table, data: []byte, piece_idx: int){
	piece := &table.entries[piece_idx]
	append(&table.append_buf, ..data)
	piece.size += len(data)
}

// Split a piece in 2 parts, one being [start:split_idx], while the other being
// [split_idx:], will never split the End piece. Returns pointers to new pieces.
@private
table_split_piece :: proc(table: ^Piece_Table, piece_idx, split_idx: int) -> (first: ^Piece, second: ^Piece) {
	pre, post : Piece
	piece := table.entries[piece_idx]
	assert(piece.kind != .EOT, "The End of table piece cannot be split.")

	// Kind is naturally preserved
	pre.kind, post.kind = piece.kind, piece.kind

	pre.start = piece.start
	pre.size  = split_idx

	post.start = pre.start + pre.size
	post.size  = piece.size - pre.size

	table.entries[piece_idx] = post
	inject_at(&table.entries, piece_idx, pre)

	first = &table.entries[piece_idx]
	second = &table.entries[piece_idx + 1]

	return
}

// Split a piece and delete a region with a size from a particular offset
@private
table_piece_delete_inner_region :: proc(
	table: ^Piece_Table,
	piece_idx,
	piece_offset,
	region_size : int) -> (first: ^Piece, second: ^Piece)
{
	piece := &table.entries[piece_idx]
	assert(piece.kind != .EOT, "Cannot delete end of table.")

	pre, post := table_split_piece(table, piece_idx, piece_offset)
	post.start += region_size
	post.size -= region_size

	first, second = pre, post
	return
}

table_read_piece :: proc(table: Piece_Table, piece_idx: int) -> []byte {
	e := table.entries[piece_idx]
	buf : []byte
	switch e.kind {
		case .Original: buf = table.original_buf
		case .Append: buf = table.append_buf[:]
		case .EOT: return nil
	}
	return buf[e.start:e.start+e.size]
}

