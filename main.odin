package text_editor

import "base:intrinsics"
import "core:mem"
import "core:fmt"
import "core:time"
import "core:thread"
import "core:sync"
import "core:math/rand"
import str "core:strings"
import os "core:os"
import ts "core:testing"

import term "ansi_term"

need_redraw := true
is_running := true

set_redraw_status :: proc(status: bool){
	intrinsics.atomic_store(&need_redraw, status)
}

render_loop :: proc(editor: ^Editor){
	for is_running {
		defer free_all(context.temp_allocator)
		if need_redraw {
			ui_reset()
			ui_render(editor^)
			need_redraw = false
			TUI_counter += 1
			fmt.println()
			display_table(editor.buffers[editor.active_buffer].text, editor.buffers[editor.active_buffer].cursor)
		}
		time.sleep(10 * time.Millisecond)
	}
}

main :: proc(){
	cur := Cursor{}
	table := table_make("Hellope")
	defer table_delete(&table)

	{ // Randomly apply a couple of splits
		for _ in 0..<10 {
			piece := rand.int_max(len(table.entries))
			off   := int(rand.int31()) % (table.entries[piece].size + 1)
			if piece == len(table.entries) - 1 {
				piece = 0
			}
			table_split_piece(&table, piece, off)
			table_gc(&table)
		}
		display_table(table)
	}

	{
		r, _ := rune_under_cursor(table, cur)
		assert(r == 'H')
	}

	{
		cur = cursor_next_rune(table, cur)
		r, _ := rune_under_cursor(table, cur)
		fmt.println("R:", r)
		assert(r == 'e')
	}
	{
		cur = cursor_prev_rune(table, cur)
		r, _ := rune_under_cursor(table, cur)
		fmt.println("R:", r)
		assert(r == 'H')
	}
	{
		cur = cursor_prev_rune(table, cur)
		r, _ := rune_under_cursor(table, cur)
		fmt.println("R:", r)
		assert(r == 'H')
	}
	{
		// cur = cursor_move_bytes(table, cur, 100)
		// r, _ := rune_under_cursor(table, cur)
		// fmt.printfln("R: %x", r)
		// assert(r == 0xfffd)
	}
}

/*
main :: proc(){
	editor := new(Editor)
	editor^ = editor_make()
	defer free(editor)

	editor_open_buffer(editor, "something.c", {})

	init_term()

	render_task := thread.create_and_start_with_poly_data(editor, render_loop)

	buf: [1024]byte
	for {
		defer free_all(context.temp_allocator)
		input := term.read(buf[:])

		need_redraw = true
		current_buf := &editor.buffers[editor.active_buffer]
		switch string(input){
		case ARROW_UP:

		case ARROW_DOWN:

		case ARROW_LEFT:
			current_buf.cursor = cursor_prev_rune(current_buf.text, current_buf.cursor)

		case ARROW_RIGHT:
			current_buf.cursor = cursor_next_rune(current_buf.text, current_buf.cursor)

		case CTRL_N:
			name := fmt.tprintf("*Scratch buffer %d*", len(editor.buffers) - 1)
			editor_open_buffer(editor, name, {.Scratch},true)

		case CTRL_S:

		case DELETE_KEY1, DELETE_KEY2:
			table_pop_rune(&current_buf.text, current_buf.cursor)

		case BACKSPACE1, BACKSPACE2:
			current_buf.cursor = cursor_prev_rune(current_buf.text, current_buf.cursor)
			table_pop_rune(&current_buf.text, current_buf.cursor)

		case END_KEY:
			current_buf.cursor = table_last(current_buf.text)

		case:
			in_place := table_add_text(&current_buf.text, input, current_buf.cursor)
			if !in_place {
				current_buf.cursor = cursor_move_bytes(current_buf.text, current_buf.cursor, len(input))
			}
		}
	}
}

ARROW_UP    :: "\e[A"
ARROW_DOWN  :: "\e[B"
ARROW_RIGHT :: "\e[C"
ARROW_LEFT  :: "\e[D"

END_KEY    :: "\e[F"
DELETE_KEY1 :: "\e[3~"
DELETE_KEY2 :: "\e[P"

BACKSPACE1 :: "\u007f"
BACKSPACE2 :: "\b"

CTRL_N :: "\x0e"
CTRL_S :: "\x13"
*/
