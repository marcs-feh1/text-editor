//+build linux
package ansi_term

import ln "core:sys/linux"
import "core:sys/unix"

foreign import tty_linux "tty_linux.o"

Fd :: ln.Fd

@(link_prefix="tty_")
foreign tty_linux {
	enable_raw_mode :: proc(fd: Fd) -> ln.Errno ---
	disable_raw_mode :: proc(fd: Fd) -> ln.Errno ---
	@(link_name="tty_get_dimensions", private="file")
	_get_dimensions :: proc(fd: Fd, width, height: ^i32) -> ln.Errno ---
}

// ioctl() request number to get terminal window size 
@(private="file")
TIOCGWINSZ :: i32(0x5413)

@(private="file")
Winsize :: struct {
	row: u16,
	col: u16,
	xpixel: u16,
	ypixel: u16,
}

STDIN_FILENO :: Fd(0)

STDOUT_FILENO :: Fd(1)

STDERR_FILENO :: Fd(2)

read :: proc(buf: []byte) -> []byte {
	n, _ := ln.read(STDIN_FILENO, buf)
	return buf[:n]
}

get_dimensions :: proc() -> (width: int, height: int) {
	tw, th: i32
	_ = _get_dimensions(STDOUT_FILENO, &tw, &th)
	width, height = int(tw), int(th)
	return
}

