package text_editor

/*
   WARNING: This "UI" is just for testing!!!
 */

import "core:fmt"
import utf "core:unicode/utf8"
import term "ansi_term"

window_width, window_height := 0, 0

init_term :: proc(){
	term.clear_screen()
	term.enable_raw_mode(term.STDIN_FILENO)
	term.reset_styling()
	window_width, window_height = term.get_dimensions()
}

deinit_term :: proc(){
	term.clear_screen()
	term.disable_raw_mode(term.STDIN_FILENO)
	term.reset_styling()
	window_width, window_height = term.get_dimensions()
}

ui_render :: proc(ed: Editor){
	stat := collect_status(ed)
	render_table(ed.buffers[ed.active_buffer].text)
	fmt.println()
	render_status(stat)
}

import "core:math/rand"

render_status :: proc(status: TUI_Status){
	term.set_color(.Cyan, .Black)
	defer term.reset_styling()
	stat := fmt.tprintf("| [%v] %v -> %v: %q", TUI_counter, status.filename, status.cursor_offset, status.hovering)
	max_width := window_width - 3
	stat_width := utf.rune_count(stat)
	if stat_width >= max_width {
		stat = stat[:max_width]
		stat_width = max_width
	}
	format := fmt.tprintf("%%s %%%ds |", max_width - stat_width)
	fmt.printfln(format, stat, "")

}

render_table :: proc(table: Piece_Table) {
	for piece in table.entries {
		buf := table.original_buf if piece.kind == .Original else table.append_buf[:]
		segment := string(buf[piece.start:(piece.start + piece.size)])
		fmt.print(segment)
	}
}

TUI_counter := 0

@(private="file")
TUI_Status :: struct {
	filename: string,
	open_buffers: int,
	cursor_offset: int,
	hovering: rune,
}

collect_status :: proc(ed: Editor) -> TUI_Status {
	cur_buf := ed.buffers[ed.active_buffer]
	hover, _ := rune_under_cursor(cur_buf.text, cur_buf.cursor)
	status := TUI_Status{
		filename = cur_buf.filename,
		open_buffers = len(ed.buffers),
		cursor_offset = offset_from_cursor(cur_buf.text, cur_buf.cursor),
		hovering = hover,
	}
	return status
}

ui_reset :: proc() {
	term.reset_styling()
	term.clear_screen()
	term.move_cursor({1, 1})
}
