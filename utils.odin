package text_editor

import "core:fmt"
import "core:mem"
import "core:unicode"

// This does **not** create a new buffer, using it to write to read-only memory
// can cause catastrophic behavior.
string_to_bytes_unsafe :: proc(s: string) -> []byte {
	return raw_data(s)[:len(s)]
}

// Clones string to byte slice
string_to_bytes_safe :: proc(s: string, allocator := context.allocator) -> (data: []byte, err: mem.Allocator_Error) {
	data = make([]byte, len(s)) or_return
	mem.copy(raw_data(data), raw_data(s), len(s))
	return
}

@(private) display_table :: proc(table: Piece_Table, cur := Cursor{}){
	r, _ := rune_under_cursor(table, cur)
	fmt.printfln("|-- TABLE [%v] -- CUR: piece=%v, off=%v, rune=%q --", table_size(table), cur.piece, cur.offset, r)
	for e, i in table.entries {
		p := table_read_piece(table, i)

		fmt.printfln("| %2d | %8s | %03d (%03d) | %q", i, e.kind, e.start, e.size, string(p))
	}
	fmt.println("|-----------")

	tmp := make([dynamic]byte)
	defer delete(tmp)

	for _, i in table.entries {
		p := table_read_piece(table, i)
		append(&tmp, ..p)
	}
	fmt.printfln("Original: %q", string(table.original_buf))
	fmt.printfln("Append: %q", string(table.append_buf[:]))
	fmt.printfln("Sequence: %q", string(tmp[:]))
}

debug_assert :: proc(cond: bool, msg: string = "", loc := #caller_location){
	when ODIN_DEBUG {
		assert(cond, msg, loc)
	}
}
